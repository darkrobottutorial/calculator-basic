package dark.robot.calculator.basic;

public interface ICalculator {

    Double add(Double left, Double right);

    Double subtract(Double left, Double right);

    Double divide(Double left, Double right);

    Double multiply(Double left, Double right);
}