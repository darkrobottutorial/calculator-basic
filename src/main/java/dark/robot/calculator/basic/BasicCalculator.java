package dark.robot.calculator.basic;

import dark.robot.calculator.AbstractCalculator;
import dark.robot.validator.IValidator;
import dark.robot.validator.Validator;

public class BasicCalculator extends AbstractCalculator implements ICalculator {

    private static BasicCalculator calculator = null;
    private IValidator validator;

    private BasicCalculator() {
        validator = Validator.getInstance();
    }

    public static BasicCalculator getInstance() {
        BasicCalculator instance = calculator;

        if (instance == null) {
            instance = new BasicCalculator();
        }

        return instance;
    }

    @Override
    public Double add(Double left, Double right) {
        validator.validate(left, right);

        return left + right;
    }

    @Override
    public Double subtract(Double left, Double right) {
        validator.validate(left, right);

        return left - right;
    }

    @Override
    public Double divide(Double left, Double right) {
        validator.validate(left, right);

        return left / right;
    }

    @Override
    public Double multiply(Double left, Double right) {
        validator.validate(left, right);

        return left * right;
    }
}