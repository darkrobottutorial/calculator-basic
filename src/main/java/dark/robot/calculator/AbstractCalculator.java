package dark.robot.calculator;

public abstract class AbstractCalculator {

    private Double memory;

    protected void saveMemory(Double value) {
        memory = value;
    }

    protected void memoryPlus() {
        memory += memory;
    }

    protected Double memoryRecall() {
        return memory;
    }

    protected void clearMemory() {
        memory = null;
    }
}