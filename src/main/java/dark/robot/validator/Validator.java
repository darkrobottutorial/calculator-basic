package dark.robot.validator;

import java.util.Map;

public class Validator implements IValidator {

    private static Validator validator = null;

    private Validator() {
        super();
    }

    public static Validator getInstance() {
        Validator instance = validator;

        if (instance == null) {
            instance = new Validator();
        }

        return instance;
    }

    @Override
    public void validate(Double target) {
        if (target == null) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void validate(Double left, Double right) {
        validate(left);
        validate(right);
    }

    @Override
    public void validate(String figureType, Map<String, Double> arguments) {
        validate(figureType);
        validate(arguments);
    }

    @Override
    public void validate(String figureType) {
        if (figureType == null) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void validate(Map<String, Double> arguments) {
        if (arguments == null || arguments.isEmpty()) {
            throw new IllegalArgumentException();
        }
    }
}