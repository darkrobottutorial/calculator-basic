package dark.robot.validator;

import java.util.Map;

public interface IValidator {
    void validate(Double target);

    void validate(Double left, Double right);

    void validate(String figureType, Map<String, Double> arguments);

    void validate(String figureType);

    void validate(Map<String, Double> arguments);
}