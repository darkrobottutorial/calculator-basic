package dark.robot.calculator.basic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BasicCalculatorTest {

    ICalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = BasicCalculator.getInstance();
    }

    @Test
    public void testAdd() throws Exception {
        Double left = 1.0;
        Double right = 2.0;
        Double expected = 3.0;

        Double actual = calculator.add(left, right);

        assertEquals(expected, actual);
    }

    @Test
    public void testSubtract() throws Exception {
        Double left = 2.0;
        Double right = 1.0;
        Double expected = 1.0;

        Double actual = calculator.subtract(left, right);

        assertEquals(expected, actual);
    }

    @Test
    public void testDivide() throws Exception {
        Double left = 2.0;
        Double right = 1.0;
        Double expected = 2.0;

        Double actual = calculator.divide(left, right);

        assertEquals(expected, actual);
    }

    @Test
    public void testMultiply() throws Exception {
        Double left = 3.0;
        Double right = 2.0;
        Double expected = 6.0;

        Double actual = calculator.multiply(left, right);

        assertEquals(expected, actual);
    }
}