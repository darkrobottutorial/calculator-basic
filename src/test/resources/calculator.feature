Feature:

  Scenario: Add two numbers
    Given the "1.0" and "2.0" numbers
    When use the calculator to add them
    Then the add result is "3.0"

  Scenario: Subtract two numbers
    Given the "2.0" and "1.0" numbers
    When use the calculator to subtract them
    Then the subtraction result is "1.0"

  Scenario: Divide two numbers
    Given the "2.0" and "1.0" numbers
    When use the calculator to divide them
    Then the division result is "2.0"

  Scenario: Multiply two numbers
    Given the "3.0" and "2.0" numbers
    When use the calculator to multiply them
    Then the multiplication result is "6.0"